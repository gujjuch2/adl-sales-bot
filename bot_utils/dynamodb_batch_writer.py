import boto3

dynamodb = boto3.resource('dynamodb')


def batch_writer(table_name,rows):
    table = dynamodb.Table(table_name)

    with table.batch_writer() as batch:
        for row in rows:
            batch.put_item(Item=row)
    return True





