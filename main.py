import logging
import boto3
import csv
from bot_utils import dynamodb_batch_writer as dbw
from datetime import datetime

logger = logging.getLogger()
logger.setLevel(logging.INFO)
job_date = str(datetime.today().strftime('%Y-%m-%d'))
job_time = str(datetime.today().strftime('%H:%M:%S'))
s3 = boto3.resource('s3')


def read_csv(csv_file, list):
    rows = csv.DictReader(open(csv_file))

    for row in rows:
        list.append(row)


def lambda_handler(event, context):
    sales_agg = 'bot_territory_month'
    file_name = 'book.csv'
    items = []

    read_csv(file_name,items)
    status = dbw.batch_writer(sales_agg, items)

    if status:
        print('Data saved to table')
    else:
        print('Failed to insert Data')

    return True

if __name__ == '__main__':
    lambda_handler('','')