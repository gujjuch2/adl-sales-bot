from setuptools import setup, find_packages
setup(
    name="adl_sales_bot",
    version='0.1.0',
    author="chaitanya gujju",
    author_email="chaitanya_gujju@epam.com",
    description="download data from open fda platform and provide as tables in athena",
    long_description_content_type="text/markdown",
    url="",
    packages=find_packages(),
    scripts=['main.py'],
    classifiers=[
        "Programming Language :: Python :: 3"
    ],
    install_requires=[
          'boto3',
      ],
    python_requires='>=3.6',
    zip_safe=False
)