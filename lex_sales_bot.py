import boto3

dynamodb = boto3.resource('dynamodb')
bot_territory_month = dynamodb.Table('bot_territory_month')


def build_response(message):
    return {
        "dialogAction": {
            "type": "Close",
            "fulfillmentState": "Fulfilled",
            "message": {
                "contentType": "PlainText",
                "content": message
            }
        }
    }


def build_key(key_id):
    return {
        'territory_month': key_id
    }


def territory_month_reply(territory_id,month,found,message):
    if found:
        return 'Territory - Sales qty :' + message.get('sa_qty') + ' , Sales Amt :' + message.get('sa_amt')
    else:
        return "Sorry, couldn't find sales details for territory_id : " \
           + territory_id + \
           ' and provided month :' \
           + month + '  combination, ' + \
           'Found all time sales information for territory : ' + territory_id + \
           '. Sales qty: ' + message.get('sa_qty') + ' ,total Sales Amt :' + message.get('sa_amt')


def get_sales_details(territory_id,month):
    territory_month = str(territory_id) + '_' + str(month)
    reply = ''
    try:
        message = bot_territory_month.get_item(Key=build_key(territory_month))['Item']
        reply = territory_month_reply(territory_id,month,True,message)
    except Exception as e:
        try:
            message = bot_territory_month.get_item(Key=build_key(territory_id))['Item']
            reply = territory_month_reply(territory_id,month,False,message)
        except Exception as ie:
            reply = "Sorry couldn't find any sales information for territory_id : " + territory_id
    return build_response(reply)


def lambda_handler(event, context):
    territory_id = event.get('currentIntent').get('slots').get('territory_id')
    month = event.get('currentIntent').get('slots').get('year_month')
    return get_sales_details(territory_id,month)
